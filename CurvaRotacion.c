#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#define SIZE 300
#define ANCHO 1000.

int cargarDatos(double *al,double *bl){
    FILE *in = fopen("RadialVelocities.dat","r");
    char c[50];
    int n;
    int k=0;
    n = fscanf(in, "%s %s\n",c,c);
    do{
        n = fscanf(in, "%lf %lf\n",&al[k],&bl[k]);
        if(n==EOF){
            break;
        }
        /*
         printf("%f,%f\n",al[k],bl[k]);
         */
        k++;
    }while(1);
    fclose(in);
    return k;
}

double rand01(){
    return (double)rand()/(double)RAND_MAX;
}

double randNormal(double ancho, double centro){
    double d1 = rand01();
    double d2 = rand01();
    
    if(d1==0){
        return centro;
    }
    else if(d1>0.5){
        return (ancho*log(d2))+centro;
    }
    else{
        return (-ancho*log(d2))+centro;
    }
}

double funcion(double *m1, double *m2, double *m3, double rad){
     double bb = 0.06235009;
     double bdad = 29.92637;
     double ah = 4134.49;
     double m11 = *m1;
     double m22 = *m2;
     double m33 = *m3;
     
     double h = ((pow(m11,0.5)*rad)/(pow(pow(rad,2)+bb,0.75)))+((pow(m22,0.5)*rad)/(pow(pow(rad,2)+bdad,0.75)))+(pow(m33,0.5)/(pow(pow(rad,2)+ah,0.25)));
     return h;
}
double similitud(double *m1, double *m2, double *m3, double *rad, double *vel){
    double a=0;
    double b;
    for(int i=0; i<SIZE; i++){
        b=funcion(m1,m2,m3,rad[i]);
        a=a+((vel[i]-b)*(vel[i]-b));
    }
    double h = exp(-(a/10e5));
    /*
    printf("%f, %f\n",a,h);
     */
    return h;
}

void Metropolis( int lanzamientos, double *rad, double *vel){
    double a = 100000*rand01();
    double b = 100000*rand01();
    double c = 100000*rand01();
    double at;
    double bt;
    double ct;
    double am = a;
    double bm = b;
    double cm = c;
    double actf = similitud(&a,&b,&c,rad,vel);
    double fm = actf;
    double tempf;
    double rand2;
    FILE *out = fopen("Max.dat","w");
    for(int i=0;i<lanzamientos;i++){
        at = randNormal(ANCHO, a);
        bt = randNormal(ANCHO, b);
        ct = randNormal(ANCHO, c);
        tempf = similitud(&at,&bt,&ct,rad,vel);
        rand2 = rand01();
        if(actf<tempf||(tempf/actf)<rand2){
            a=at;
            b=bt;
            c=ct;
            actf=tempf;
            fprintf(out,"%f %f %f %f\n",at,bt,ct,tempf);
            if(actf>fm){
                fm = actf;
                am=a;
                bm=b;
                cm=c;
            }
        }
    }
    fclose(out);
    out = fopen("Max1.dat","w");
    fprintf(out,"%f %f %f %f\n",am,bm,cm,fm);
    printf("Mb=%f, Md=%f, Mh=%f\n",am,bm,cm);
    fclose(out);
}

int main(){
    
    double rad[SIZE];
    double vel[SIZE];
    cargarDatos(rad,vel);
    
    Metropolis( 100000, rad, vel);
    
    return 0;
    
}

