import numpy as np
import matplotlib.pyplot as plt

dat = np.genfromtxt("RadialVelocities.dat", delimiter=" ", dtype="float")

plt.scatter(dat[:,0],dat[:,1], s=15, marker='p', c='r',alpha=0.5,label="Observados")

bb = 0.06235009
bdad = 29.92637
ah = 4134.49
rad = np.linspace(0,300,300)

plt.scatter(dat[:,0],dat[:,1], s=15, marker='p', c='r',alpha=0.5)
dat = np.genfromtxt("Max1.dat", delimiter=" ", dtype="float")
m11 = dat[0]
m22 = dat[1]
m33 = dat[2]
rad = np.linspace(0,300,300)
ve = (((m11**(0.5)*rad)/((rad**2+bb)**(0.75)))+((m22**(0.5)*rad)/((rad**2+bdad)**0.75))+(m33**(0.5)/((rad**2+ah)**0.25)))

plt.plot(rad,ve,label="Ajuste")
plt.title("Ajuste por Metropolis–Hastings")
plt.legend()
plt.xlabel("Radios(kpc)")
plt.ylabel("Velocidad(km/s)")
plt.savefig("Grafica.png",dpi=500,bbox_inches='tight')
plt.close()



dat = np.genfromtxt("Max.dat", delimiter=" ", dtype="float")
plt.scatter(dat[:,0], -np.log(dat[:,3]),alpha=0.2,s=3)
plt.title("Chi cudrado vs Mb")
plt.xlabel("Chi cudrado")
plt.ylabel("Mb")
plt.savefig("M1.png",dpi=500,bbox_inches='tight')
plt.close()
plt.scatter(dat[:,1], -np.log(dat[:,3]),alpha=0.2,s=3)
plt.title("Chi cudrado vs Md")
plt.xlabel("Chi cudrado")
plt.ylabel("Md")
plt.savefig("M2.png",dpi=500,bbox_inches='tight')
plt.close()
plt.scatter(dat[:,2], -np.log(dat[:,3]),alpha=0.2,s=3)
plt.title("Chi cudrado vs Mh")
plt.xlabel("Chi cudrado")
plt.ylabel("Mh")
plt.savefig("M3.png",dpi=500,bbox_inches='tight')
plt.close()


